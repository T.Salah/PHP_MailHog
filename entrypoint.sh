#!/bin/bash

# Dynamically update /etc/hosts with the IP address of the 'mailhog-service' using Kubernetes DNS resolution
echo "$(nslookup mailhog-service | grep 'Address:' | tail -n1 | awk '{print $2}') mailhog-service" >> /etc/hosts
# Other setup or commands if needed
exec "$@"
