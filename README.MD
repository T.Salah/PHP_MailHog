![Send Emails interface](images/image_0.png)

# PHP Email App

> This project allows you to test email sending using PHP and MailHog. It includes Dockerfiles for both local development and Kubernetes deployment.

## Requirements

Before you begin, ensure that you have the following prerequisites installed on your local machine:

- [Docker](https://www.docker.com/get-started) for building and running containers.
- [kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/) for interacting with Kubernetes clusters (if deploying to Kubernetes).
- Access to a Kubernetes cluster (if deploying to Kubernetes).

Ensure that the required ports are available:

- Port `8080` for accessing the PHP Email App locally.
- Port `8025` for accessing MailHog locally.


To launch the project using Docker and Docker Compose, navigate to the project's root directory and execute the following command:

`docker-compose up -d --build`


After executing the command, verify the status of your containers using the following command:

`docker compose ps -a`

After executing the commands, you should observe output similar to the following:


| CONTAINER ID   | IMAGE                 | COMMAND                   | CREATED             | STATUS             | PORTS                                            | NAMES                  |
|----------------|-----------------------|---------------------------|---------------------|--------------------|--------------------------------------------------|------------------------|
| 27e89ed5b3bd   | email_app_php         | "docker-php-entrypoi…"    | About an hour ago   | Up About an hour   | 0.0.0.0:8080->80/tcp, :::8080->80/tcp            | email_app-php-1       |
| 3c3993dd2d24   | mailhog/mailhog:latest| "MailHog"                 | About an hour ago   | Up About an hour   | 1025/tcp, 0.0.0.0:8025->8025/tcp, :::8025->8025/tcp | email_app-mailhog-1   |



Open your browser and go to http://localhost:8080. You should see the PHP Email App form. Enter the email details and click "Send Email."

![Send Emails interface](images/image_1.png)

![Success message](images/image_2.png)


Open your browser and go to http://localhost:8025 to access the MailHog web interface. You'll see the sent emails there.

![MailHog interface](images/image_3.png)

# Deployment to Kubernetes Instructions
> If you wish to deploy this project to Kubernetes, ensure you have a functional Kubernetes cluster using tools such as Kubespray, Kubeadm, or Minikube. Follow the instructions below for a seamless deployment experience:

#### Rebuild and Push the PHP Email Image:
> Before proceeding, rebuild the PHP email image and push it to your Docker registry (DockerHub, GitLab Registry, etc.). Alternatively, you can use the pre-built Docker image available at:

`salahtouil/email-php-app:latest`

If opting for your own build, ensure to update line `17` in `php-deployment.yaml` with your `image name`.

__Pro Tip__: Simplify image building for Kubernetes by modifying line 8 in docker-compose.yaml:

`dockerfile: docker/php/Dockerfile`
__to__ 
`dockerfile: docker/php/Dockerfile.kubernetes`


Next navigate to the manifests directory and execute the following command:

`kubectl apply -f mailhog-deployment.yaml`
`kubectl apply -f php-deployment.yaml`

#### Verify Deployment:

Confirm the successful deployment by running:

`kubectl get all`

You should observe output similar to the following:

| NAME                                   | READY | STATUS  | RESTARTS | AGE  |
|----------------------------------------|-------|---------|----------|------|
| mailhog-deployment-586b689d47-kg8rs    | 1/1   | Running | 0        | 5h   |
| php-deployment-59ccd6fb6-wts25         | 1/1   | Running | 0        | 101m |

### Services

| NAME                | TYPE      | CLUSTER-IP      | EXTERNAL-IP | PORT(S)                           | AGE   |
|---------------------|-----------|-----------------|-------------|-----------------------------------|-------|
| mailhog-service     | NodePort  | 10.43.255.251   | \<none\>    | 8025:31605/TCP,1025:30682/TCP     | 126m  |
| php-service         | NodePort  | 10.43.159.150   | \<none\>    | 80:32707/TCP                      | 5h2m  |

### Deployments

| NAME                          | READY | UP-TO-DATE | AVAILABLE | AGE  |
|-------------------------------|-------|------------|-----------|------|
| mailhog-deployment            | 1/1   | 1          | 1         | 5h2m |
| php-deployment                | 1/1   | 1          | 1         | 101m |


# Application Testing:

>>Open your web browser and navigate to http://localhost:8080. You should encounter the PHP Email App form. Fill in the email details and click "Send Email" for a comprehensive application test.

>>Open your web browser and navigate to http://localhost:8025 to access MailHog. Verify that the emails are correctly captured and displayed in the MailHog interface.