<!-- html/send_email.php -->

<?php
# This 2 variables for Kubernetes.
$mailhogHost = "mailhog-service";
$mailhogPort = 1025;  // Assuming this is your SMTP port

if ($_SERVER["REQUEST_METHOD"] === "POST") {
    $to = $_POST["to"];
    $subject = $_POST["subject"];
    $message = $_POST["message"];

    // Use msmtp to send email (configured in Dockerfile)
    $command = "echo \"$message\" | msmtp -- -a default -t $to";
    exec($command, $output, $status);

    // Check the status of the email sending
    $successMessage = "Email sent successfully!";
    $errorMessage = "Failed to send email. Please try again.";

    // Display the appropriate message with styling
    if ($status === 0) {
        $messageToShow = '<p style="color: #4caf50; font-size: 18px;">✓ ' . $successMessage . '</p>';
    } else {
        $messageToShow = '<p style="color: #f44336; font-size: 18px;">✗ ' . $errorMessage . '</p>';
    }

    // Display the message and buttons
    echo '<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Email Status</title>
    <style>
        body {
            font-family: \'Arial\', sans-serif;
            background-color: #f5f5f5;
            margin: 0;
            padding: 0;
            display: flex;
            align-items: center;
            justify-content: center;
            height: 100vh;
            flex-direction: column; /* Added to align content in a column */
        }

        p {
            text-align: center;
        }

        button {
            background-color: #4caf50;
            color: #fff;
            padding: 10px 15px;
            border: none;
            border-radius: 4px;
            cursor: pointer;
            font-size: 16px;
            margin: 10px;
        }
    </style>
</head>
<body>
    ' . $messageToShow . '
    <button onclick="goBack()">Go Back</button>
    <button onclick="openMailhog()">Open MailHog</button>

    <script>
        function goBack() {
            window.history.back();
        }

        function openMailhog() {
            window.open("http://localhost:8025/");
        }
    </script>
</body>
</html>';
}
?>
